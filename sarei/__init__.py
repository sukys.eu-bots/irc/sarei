# coding=utf-8
"""
daylt bot
"""
import datetime

import sopel.module
import sopel.formatting
from sopel.tools import SopelMemory

def extract_date(trigger) -> datetime.date: 
    _, year, month, day = trigger.groups()
    extracted_date = datetime.date(int(year), int(month), int(day))
    return extracted_date

def calculate_date_diff(date: datetime.date) -> datetime.timedelta:
    today = datetime.datetime.now().date()
    diff = today - date
    if diff < datetime.timedelta(0): # it's in the future
        raise ValueError('pasikviesi kartu gerti? :-)')
    if diff == datetime.timedelta(0): # it's today
        raise ValueError('ktunx, geri?')
    return diff

def format_message(name: str, diff: datetime.timedelta) -> str:
    LOWERCASED_NAMES = {
        "bugie": "apsimyžęs chronius netgi",
        "sarei": "Motiejus Valančius vis dar"
    }
    if name.lower() in LOWERCASED_NAMES:
        msg = LOWERCASED_NAMES[name.lower()] + " "
    else:
        msg = "blaivas netgi "
    msg += sopel.formatting.bold(str(diff.days) + ' d.')
    return msg

@sopel.module.rate(60,60,60)
@sopel.module.rule('\[(\w+)\]\s+(\d+)[^\d]+(\d+)[^\d]+(\d+)')
def sarei(bot, trigger):
    try:
        name, *_ = trigger.groups()
        date = extract_date(trigger)
        diff = calculate_date_diff(date)
        message = format_message(name, diff)
        bot.say(message)
    except ValueError as e:
        bot.say(str(e))


@sopel.module.commands('geriau')
def geriau(bot, trigger):
    if trigger.nick not in bot.memory:
        bot.memory[trigger.nick] = SopelMemory()
    bot.memory[trigger.nick] = datetime.datetime.now().date()
    bot.say('ok')

@sopel.module.commands('blaivas')
def blaivas(bot, trigger):
    if trigger.nick not in bot.memory:
        bot.memory[trigger.nick] = SopelMemory()
        bot.memory[trigger.nick] = datetime.datetime.now().date()
        bot.say('Nuo šiandien tu blaivas :]')
    else:
        try:
            diff = calculate_date_diff(bot.memory[trigger.nick])
            bot.say('Blaivybėje viso labo ' + diff.days + ' d.')
        except ValueError:
            bot.say('Kur skubi, nuo šiandien gi blaivas')
