import setuptools

setuptools.setup(
    name="sarei",
    author="Paulius",
    author_email="paulius@sukys.eu",
    description="legacy botas dienom skaičiuoti",
    use_scm_version=True,
    packages=setuptools.find_packages(),
    install_requires=[
        "sopel"
    ],
    setup_requires=[
        "setuptools_scm"
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    entry_points={
        "sopel.plugins": ["sarei=sarei"]  
    },
    python_requires='>=3.6',
)
